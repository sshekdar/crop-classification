import numpy as np
import cv2
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.neural_network import MLPClassifier
import train
import joblib

def classify(pad_image, output_filename, height, width):
    print("Loading Model")
    #model = train.train()
    model = joblib.load('final_model.sav')
    print("Completed Loading Model")
    print("Classifying Model")
    output = None
    one_count = 0
    zero_count = 0
    if model.predict(np.array([pad_image[0 : height, 0 : width, :].ravel()])) == 1:
        output = np.ones((height, width)) * 100
        one_count = one_count + 1
    else:
        output = np.zeros((height, width))
        zero_count = zero_count + 1
    for j in range(width, pad_image.shape[1], width):
        if model.predict(np.array([pad_image[0 : height, j : j + width, :].ravel()])) == 1:
            output = np.concatenate((output, np.ones((height, width)) * 100), axis = 1)
            one_count = one_count + 1
        else:
            output = np.concatenate((output, np.zeros((height, width))), axis = 1)
            zero_count = zero_count + 1
    for i in range(height, pad_image.shape[0], height):
        if i % 1000 == 0:
            print("Row {}".format(i))
        row = None
        if model.predict(np.array([pad_image[i : i + height, 0 : width, :].ravel()])) == 1:
            one_count = one_count + 1
            row = np.ones((height, width)) * 100
        else:
            row = np.zeros((height, width))
            zero_count = zero_count + 1
        for j in range(width, pad_image.shape[1], width):
            if model.predict(np.array([pad_image[i : i + height, j : j + width, :].ravel()])) == 1:
                one_count = one_count + 1
                row = np.concatenate((row, np.ones((height, width)) * 100), axis = 1)
            else:
                row = np.concatenate((row, np.zeros((height, width))), axis = 1)
                zero_count = zero_count + 1
        output = np.concatenate((output, row), axis = 0)
    
    print("Classified Negative {}, Positive{}".format(zero_count, one_count))
    print("Writing Image")
    cv2.imwrite(output_filename, output)
    print("Done writing image")