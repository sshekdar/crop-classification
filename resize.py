import os
os.environ["OPENCV_IO_MAX_IMAGE_PIXELS"] = pow(2,40).__str__()
import cv2

def resize_single(image_filename, width, height):
    img = cv2.imread(image_filename.decode("utf-8"), cv2.IMREAD_UNCHANGED)

    print('Original Dimensions : ',img.shape)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    
    print('Resized Dimensions : ',resized.shape)
    cv2.imwrite(image_filename.decode("utf-8"), resized)

def resize_single_percent(image_filename, scale_percent):
    img = cv2.imread(image_filename, cv2.IMREAD_UNCHANGED)

    width = int(img.shape[1] * scale_percent / 100)
    height = int(img.shape[0] * scale_percent / 100)
    print('Original Dimensions : ',img.shape)
    dim = (width, height)
    # resize image
    resized = cv2.resize(img, dim, interpolation = cv2.INTER_AREA)
    
    print('Resized Dimensions : ',resized.shape)
    cv2.imwrite(image_filename, img)

def resize_folder(directory_in_str, width, height):
    directory = os.fsencode(directory_in_str)
    for filename in os.listdir(directory):
        if filename.endswith(b".jpg") or filename.endswith(b".png") or filename.endswith(b".PNG") or filename.endswith(b".JPG"):
            print(os.path.join(directory, filename))
            resize_single(os.path.join(directory, filename), width, height)
        else:
            continue

resize_folder("Cotton Images", 100, 100)