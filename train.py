import numpy as np
import cv2
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.neural_network import MLPClassifier
import joblib

def train():
    train_labels = np.zeros(65)
    train_labels = np.concatenate((train_labels, np.ones(13)), axis = 0)
    train_labels = np.concatenate((train_labels, np.zeros(62)), axis = 0)
    train_labels = np.concatenate((train_labels, np.ones(13)), axis = 0)
    train_labels = np.concatenate((train_labels, np.zeros(58)), axis = 0)
    train_labels = np.concatenate((train_labels, np.ones(13)), axis = 0)

    observations = np.array([cv2.imread("images/0.jpg").ravel()])
    for i in range(1, 65):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)
    for i in range(65, 78):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)
    for i in range(78, 140):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)
    for i in range(143, 156):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)
    for i in range(160, 218):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)
    for i in range(221, 234):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)

    print(observations.shape)
    neural_net_clf = MLPClassifier(alpha=1, max_iter=1000)
    neural_net_clf.fit(observations, train_labels)
    print("Accuracy: ", neural_net_clf.score(observations, train_labels))
    joblib.dump(neural_net_clf, 'final_model.sav')
    return neural_net_clf

