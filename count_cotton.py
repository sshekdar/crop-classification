import numpy as np
import cv2
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.neural_network import MLPClassifier

def train():
    train_labels = np.zeros(78)
    train_labels = np.concatenate((train_labels, np.ones(28)), axis = 0)

    observations = np.array([cv2.imread("images/0.jpg").ravel()])
    for i in range(1, 78):
        image = np.array([cv2.imread("images/" + str(i) + ".jpg").ravel()])
        observations = np.concatenate((observations, image), axis = 0)
    
    for i in range(3, 31):
        image = np.array([cv2.imread("Cotton Images/" + str(i) + ".PNG").ravel()])
        observations = np.concatenate((observations, image), axis = 0)

    print(observations.shape)
    neural_net_clf = MLPClassifier(alpha=1, max_iter=1000)
    neural_net_clf.fit(observations, train_labels)
    print("Accuracy: ", neural_net_clf.score(observations, train_labels))
    return neural_net_clf

def classify(pad_image, height, width):
    print(pad_image.shape)
    model = train()
    output = None
    one_count = 0
    zero_count = 0
    if model.predict(np.array([pad_image[0 : height, 0 : width, :].ravel()])) == 1:
        output = np.ones((height, width)) * 100
        one_count = one_count + 1
    else:
        output = np.zeros((height, width))
        zero_count = zero_count + 1
    for j in range(width, pad_image.shape[1], width):
        if model.predict(np.array([pad_image[0 : height, j : j + width, :].ravel()])) == 1:
            output = np.concatenate((output, np.ones((height, width)) * 100), axis = 1)
            one_count = one_count + 1
        else:
            output = np.concatenate((output, np.zeros((height, width))), axis = 1)
            zero_count = zero_count + 1
    for i in range(height, pad_image.shape[0], height):
        row = None
        if model.predict(np.array([pad_image[i : i + height, 0 : width, :].ravel()])) == 1:
            one_count = one_count + 1
            row = np.ones((height, width)) * 100
        else:
            row = np.zeros((height, width))
            zero_count = zero_count + 1
        for j in range(width, pad_image.shape[1], width):
            if model.predict(np.array([pad_image[i : i + height, j : j + width, :].ravel()])) == 1:
                one_count = one_count + 1
                row = np.concatenate((row, np.ones((height, width)) * 100), axis = 1)
            else:
                row = np.concatenate((row, np.zeros((height, width))), axis = 1)
                zero_count = zero_count + 1
        output = np.concatenate((output, row), axis = 0)
    
    cv2.imwrite("output.jpg", output)
    print(zero_count, one_count)

image = cv2.imread("padded_image.jpg")
classify(image, 100, 100)