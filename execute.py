import split
import classify
import os
os.environ["OPENCV_IO_MAX_IMAGE_PIXELS"] = pow(2,40).__str__()
import cv2
def classify_folder(directory_in_str, height, width):
    directory = os.fsencode(directory_in_str)
    for filename in os.listdir(directory):
        if filename.endswith(b".jpg") or filename.endswith(b".png") or filename.endswith(b".PNG") or filename.endswith(b".JPG"):
            classify.classify(split.pad_image(directory_in_str + "\\" + filename.decode("utf-8") , height, width), "Final_Ortho_Outputs\out_" + filename.decode("utf-8"), height, width)
        else:
            continue

#classify_folder("Final_Ortho_Tiles", 100, 100)
classify.classify(split.pad_image("sample.jpg", 100, 100), "big_result.jpg", 100, 100)