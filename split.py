import numpy as np
import os
os.environ["OPENCV_IO_MAX_IMAGE_PIXELS"] = pow(2,40).__str__()
import cv2
import matplotlib.pyplot as plt
import pandas as pd

def split_image(image_name, height, width):
    original_image = cv2.imread(image_name)

    height_pad = height - original_image.shape[0] % height
    width_pad = width - original_image.shape[1] % width
    height_pad_arr = np.zeros((height_pad, original_image.shape[1], original_image.shape[2]), dtype = int)
    width_pad_arr = np.zeros((original_image.shape[0] + height_pad, width_pad, original_image.shape[2]), dtype = int)
    pad_image = np.concatenate((original_image, height_pad_arr), axis = 0)
    pad_image = np.concatenate((pad_image, width_pad_arr), axis = 1)
    cv2.imwrite('padded_image.jpg', pad_image)

    split_images = []
    image_id = 0
    for i in range(0, pad_image.shape[0], height):
        row = []
        for j in range(0, pad_image.shape[1], width):
            image = pad_image[i : i + height, j : j + width, :]
            #cv2.imwrite(('images/' + str(image_id) + '.jpg'), image)
            cv2.imwrite(('large_ortho_images/' + str(image_id) + '.jpg'), image)
            row.append({"id" : image_id, "image" : pad_image[i : i + height, j : j + width, :]})
            image_id = image_id + 1
        split_images.append(row)
    df = pd.DataFrame(split_images)
    df.to_csv("result.csv", index = False)
    print(original_image.shape)
    return pad_image

def pad_image(image_name, height, width):
    print("Opening Image " + image_name)
    original_image = cv2.imread(image_name)
    print("Completed Opening Image")
    print("Input Image {}, {}".format(original_image.shape[0], original_image.shape[1]))
    print("Padding Image")
    height_pad = height - original_image.shape[0] % height
    width_pad = width - original_image.shape[1] % width
    height_pad_arr = np.zeros((height_pad, original_image.shape[1], original_image.shape[2]), dtype = int)
    width_pad_arr = np.zeros((original_image.shape[0] + height_pad, width_pad, original_image.shape[2]), dtype = int)
    pad_image = np.concatenate((original_image, height_pad_arr), axis = 0)
    pad_image = np.concatenate((pad_image, width_pad_arr), axis = 1)
    print("Completed Padding Image")
    print("Padded Image {}, {}".format(pad_image.shape[0], pad_image.shape[1]))
    return pad_image


